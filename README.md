# ocaml-jubjub

OCaml implementation of the Jubjub curve

## Install

1. Setup environment
```
opam switch create ./ 4.09.1 --deps-only
eval $(opam env)
```

2. Install Rust dependencies and install the library


```
./build_deps.sh
opam install . -y
```

3. Play with utop

```
opam install utop
dune utop
```

## Run tests

```
dune build @install
opam install alcotest
dune runtest
```

## Documentation

- Use `dune build @doc` to generate the API documentation.
