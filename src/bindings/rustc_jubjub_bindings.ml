open Ctypes

module Bindings (F : Cstubs.FOREIGN) = struct
  open F

  let check_bytes =
    foreign "rustc_jubjub_check_bytes" (ocaml_bytes @-> returning bool)

  let one = foreign "rustc_jubjub_one" (ocaml_bytes @-> returning void)

  let zero = foreign "rustc_jubjub_zero" (ocaml_bytes @-> returning void)

  let add =
    foreign
      "rustc_jubjub_add"
      (ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> returning void)

  let negate =
    foreign
      "rustc_jubjub_negate"
      (ocaml_bytes @-> ocaml_bytes @-> returning void)

  let double =
    foreign
      "rustc_jubjub_double"
      (ocaml_bytes @-> ocaml_bytes @-> returning void)

  let eq =
    foreign "rustc_jubjub_eq" (ocaml_bytes @-> ocaml_bytes @-> returning bool)

  let is_zero = foreign "rustc_jubjub_is_zero" (ocaml_bytes @-> returning bool)

  let mul =
    foreign
      "rustc_jubjub_mul"
      (ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> returning void)

  let get_u_coordinate =
    foreign
      "rustc_jubjub_get_u_coordinate"
      (ocaml_bytes @-> ocaml_bytes @-> returning void)

  let get_v_coordinate =
    foreign
      "rustc_jubjub_get_v_coordinate"
      (ocaml_bytes @-> ocaml_bytes @-> returning void)

  let from_coordinates =
    foreign
      "rustc_jubjub_from_coordinates"
      (ocaml_bytes @-> ocaml_bytes @-> ocaml_bytes @-> returning void)

  let is_torsion_free =
    foreign "rustc_jubjub_is_torsion_free" (ocaml_bytes @-> returning bool)

  let multiply_by_cofactor =
    foreign
      "rustc_jubjub_multiply_by_cofactor"
      (ocaml_bytes @-> ocaml_bytes @-> returning void)
end
