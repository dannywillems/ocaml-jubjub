module Jubjub_stubs = Rustc_jubjub_bindings.Bindings (Rustc_jubjub_stubs)

module Fr = Ff.MakeFp (struct
  let prime_order =
    Z.of_string
      "6554484396890773809930967563523245729705921265872317281365359162392183254199"
end)

module Scalar = Fr

module Fq = Ff.MakeFp (struct
  let prime_order =
    Z.of_string
      "52435875175126190479447740508185965837690552500527637822603658699938581184513"
end)

module BaseField = Fq

exception Not_on_curve of Bytes.t

(* let a = Fq.negate Fq.one *)

let d = Fq.(negate (of_string "10240") / of_string "10241")

(** The type of the element in the elliptic curve *)
type t = Bytes.t

let size_in_bytes = 32

let empty () = Bytes.make size_in_bytes '\000'

let check_bytes bs =
  if Bytes.length bs = size_in_bytes then
    Jubjub_stubs.check_bytes (Ctypes.ocaml_bytes_start bs)
  else false

let of_bytes_opt b = if check_bytes b then Some b else None

let of_bytes_exn b = if check_bytes b then b else raise (Not_on_curve b)

let to_bytes b = b

let zero =
  let buffer = empty () in
  Jubjub_stubs.zero (Ctypes.ocaml_bytes_start buffer) ;
  buffer

let one =
  let buffer = empty () in
  Jubjub_stubs.one (Ctypes.ocaml_bytes_start buffer) ;
  buffer

let is_zero g = Jubjub_stubs.is_zero (Ctypes.ocaml_bytes_start g)

let from_coordinates_opt ~u ~v =
  let buffer = empty () in
  Jubjub_stubs.from_coordinates
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start (Fq.to_bytes u))
    (Ctypes.ocaml_bytes_start (Fq.to_bytes v)) ;
  of_bytes_opt buffer

let from_coordinates_exn ~u ~v =
  let buffer = empty () in
  Jubjub_stubs.from_coordinates
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start (Fq.to_bytes u))
    (Ctypes.ocaml_bytes_start (Fq.to_bytes v)) ;
  of_bytes_exn buffer

let get_u_coordinate g =
  let buffer = Bytes.make Fq.size_in_bytes '\000' in
  Jubjub_stubs.get_u_coordinate
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g) ;
  Fq.of_bytes_exn buffer

let get_v_coordinate g =
  let buffer = Bytes.make Fq.size_in_bytes '\000' in
  Jubjub_stubs.get_v_coordinate
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g) ;
  Fq.of_bytes_exn buffer

let to_string g =
  Printf.sprintf
    "JubjubAffine(u = %s, v = %s)"
    (Fq.to_string (get_u_coordinate g))
    (Fq.to_string (get_v_coordinate g))

let random ?state () =
  ignore state ;
  let rec aux () =
    let u = Fq.random () in
    let v = Fq.random () in
    let res = from_coordinates_opt ~u ~v in
    match res with Some res -> res | None -> aux ()
  in
  aux ()

let add g1 g2 =
  let buffer = empty () in
  Jubjub_stubs.add
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g1)
    (Ctypes.ocaml_bytes_start g2) ;
  buffer

let double g =
  let buffer = empty () in
  Jubjub_stubs.double
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g) ;
  buffer

let negate g =
  let buffer = empty () in
  Jubjub_stubs.negate
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g) ;
  buffer

let eq g1 g2 =
  Jubjub_stubs.eq (Ctypes.ocaml_bytes_start g1) (Ctypes.ocaml_bytes_start g2)

let mul g (a : Scalar.t) =
  let buffer = empty () in
  Jubjub_stubs.mul
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g)
    (Ctypes.ocaml_bytes_start (Scalar.to_bytes a)) ;
  buffer

let is_torsion_free g =
  Jubjub_stubs.is_torsion_free (Ctypes.ocaml_bytes_start g)

let multiply_by_cofactor g =
  let buffer = empty () in
  Jubjub_stubs.multiply_by_cofactor
    (Ctypes.ocaml_bytes_start buffer)
    (Ctypes.ocaml_bytes_start g) ;
  buffer
