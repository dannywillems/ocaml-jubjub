(** NB: A point is always in the prime subgroup. Affine coordinates are used by default but are hidden within the type. *)
include Ec.Elliptic_curve.AffineTwistedEdwardsT

val multiply_by_cofactor : t -> t

(** [is_torsion_free p] returns [true] if p is in the subgroup, [false] otherwise. *)
val is_torsion_free : t -> bool

val to_string : t -> string
