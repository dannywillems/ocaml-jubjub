(** Check the routine generators do not raise any exception *)
module ValueGeneration = Test_ec_make.MakeValueGeneration (Jubjub)

module IsZero = Test_ec_make.MakeIsZero (Jubjub)
module Equality = Test_ec_make.MakeEquality (Jubjub)
module ECProperties = Test_ec_make.MakeECProperties (Jubjub)

(* module TestVectors = struct
 *   let test_vectors_addition () =
 *     let u = Jubjub.BaseField.of_bytes_exn (Hex. (`Hex 0x81c571e5d883cfb0049f7a686f147029f539c860bc3ea21f4284715b7ccc8162));
 *     let v = Jubjub.BaseField.of_bytes_exn (Hex.of_bytes (`Hex 0xbf096275684bb8cac7ba245890af256d59119f3e86380eb03793de182f9fb1d2));
 * end *)

let () =
  let open Alcotest in
  Random.self_init () ;
  run
    "Jubjub"
    [ IsZero.get_tests ();
      ValueGeneration.get_tests ();
      Equality.get_tests ();
      ECProperties.get_tests () ]
